use yew::prelude::*;

enum Msg {
    SubmitAnswer(usize),
}

struct Question {
    question: String,
    image: String,
    answers: Vec<String>,
    correct_answer_index: usize,
}

impl Question {
    fn correct_answer(&self) -> &str {
        self.answers.get(self.correct_answer_index).unwrap()
    }
}

struct QuizzComponent {
    questions: Vec<Question>,
    current_question_index: usize,
    user_answers: Vec<usize>,
}

impl QuizzComponent {
    fn score(&self) -> usize {
        let correct_answers: Vec<usize> = self
            .questions
            .iter()
            .map(|q| q.correct_answer_index)
            .collect();

        let mut score: usize = 0;

        for (idx, correct_answer) in correct_answers.iter().enumerate() {
            if let Some(user_answer) = self.user_answers.get(idx) {
                if correct_answer == user_answer {
                    score += 1;
                }
            }
        }

        score
    }
}

impl Component for QuizzComponent {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            questions: vec![
                Question {
                    question: String::from("What element does the chemical symbol Au stand for?"),
                    answers: vec![
                        String::from("Copper"),
                        String::from("Gold"),
                        String::from("Aluminum"),
                    ],
                    correct_answer_index: 1,
                    image: String::from("au.png"),
                },
                Question {
                    question: String::from("Which country invented tea?"),
                    answers: vec![
                        String::from("India"),
                        String::from("Korea"),
                        String::from("China"),
                    ],
                    correct_answer_index: 2,
                    image: String::from("tea.jpg"),
                },
                Question {
                    question: String::from("What is the longest running Broadway show?"),
                    answers: vec![
                        String::from("The Lion King"),
                        String::from("The Phantom of the Opera"),
                        String::from("Cats"),
                    ],
                    correct_answer_index: 1,
                    image: String::from("broadway_show.jpg"),
                },
            ],
            current_question_index: 0,
            user_answers: vec![],
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SubmitAnswer(user_answer) => {
                self.user_answers.push(user_answer);
                self.current_question_index += 1;
                true // re-render component
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let alphabet: Vec<char> = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().collect();

        match self.questions.get(self.current_question_index) {
            Some(current) => {
                let question = &current.question;
                let answers = &current.answers;
                let image = &current.image;

                html! {
                    <div class="container">
                    <img class="center" src={format!("img/{}", image.clone())}/>
                        <p class="huge">{ question }</p>
                        <div id="answers">

                    {
                        answers.iter().enumerate().map(|(index, answer)| {
                            html!{<p><button onclick={link.callback(move |_| Msg::SubmitAnswer(index))}>{ format!("{}. {}", alphabet.get(index).unwrap(), answer) }</button></p>}
                        }).collect::<Html>()
                    }
                    </div>
                    </div>
                }
            }
            _ => {
                let questions = &self.questions;
                html! {
                    <div class="container">
                    if self.score() > 0 {
                        <p class="huge">{ format!("Congratulations! Your Score is {} 🎉!", self.score()) }</p>
                    } else {
                        <p class="huge">{ format!("You've completed the quizz. Unfortunately, your Score is {} 😢", self.score()) }</p>
                    }

                    {
                        questions.iter().enumerate().map(|(index,question)| {
                            let correct = *self.user_answers.get(index).unwrap() == question.correct_answer_index;
                            let p_class = if correct { "correct" } else { "incorrect" };


                            html!{<p class={p_class}>{ format!("The correct answer for '{}' is '{}'", question.question.clone(), question.correct_answer())}</p>}
                        }).collect::<Html>()
                    }

                    </div>
                }
            }
        }
    }
}

fn main() {
    yew::start_app::<QuizzComponent>();
}
